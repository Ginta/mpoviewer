﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;
using System.Threading.Tasks;
using Windows.ApplicationModel;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage;
using Windows.Storage.Search;
using Windows.Storage.Streams;
using Windows.UI.Popups;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=391641

namespace MPOViewer
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {

        private IReadOnlyList<StorageFile> mpoFileList;
        private DispatcherTimer timer;
        private int currentPictureIndex;

        public MainPage()
        {
            this.InitializeComponent();

            this.NavigationCacheMode = NavigationCacheMode.Required;

            var app = App.Current as App;
            image1.Width = app.CalculateIpdViewPixels();
            image2.Width = app.CalculateIpdViewPixels();

           // image1.Source = new BitmapImage(new Uri("D:\\Pictures\\Camera Roll\\WP_20160102_001.jpg", UriKind.Absolute));
           // image2.Source = new BitmapImage(new Uri("D:\\Pictures\\Camera Roll\\WP_20160102_001.jpg", UriKind.Absolute));

            hide_bar();
        }

        public async void hide_bar()
        {
            StatusBar statusBar = StatusBar.GetForCurrentView();
            await statusBar.HideAsync();
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.
        /// This parameter is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            // TODO: Prepare page for display here.

            // TODO: If your application contains multiple pages, ensure that you are
            // handling the hardware Back button by registering for the
            // Windows.Phone.UI.Input.HardwareButtons.BackPressed event.
            // If you are using the NavigationHelper provided by some templates,
            // this event is handled for you.
        }

        private void image1_Loaded(object sender, RoutedEventArgs e)
        {
            var app = App.Current as App;
            //image1.Height = app.CalculateIpdViewPixels() / 2;
        }

        private void image2_Loaded(object sender, RoutedEventArgs e)
        {
            var app = App.Current as App;
            //image1.Height = app.CalculateIpdViewPixels() / 2;
        }

        private async void Page_Loaded(object sender, RoutedEventArgs e)
        {
            //var fileList = ListMpoFiles().Result;
            mpoFileList = await ListMpoFiles();
            if (mpoFileList != null && mpoFileList.Count > 0)
            {
                await OpenMpoFile(mpoFileList[0].Path);
                currentPictureIndex = 0;
                timer = new DispatcherTimer();
                timer.Tick += onTimerTick;
                timer.Interval = new TimeSpan(0, 0, 5);
                timer.Start();
            }
        }

        private async void onTimerTick(object sender, object e)
        {
            ++currentPictureIndex;
            if (mpoFileList != null && currentPictureIndex < mpoFileList.Count)
            {
                await OpenMpoFile(mpoFileList[currentPictureIndex].Path);
            }
            else
            {
                currentPictureIndex = 0;
            }
        }

        private async Task<IReadOnlyList<StorageFile>> ListMpoFiles()
        {
            try {
                // https://msdn.microsoft.com/en-US/library/windows/apps/xaml/dn611857.aspx

               // // Get the logical root folder for all external storage devices.
                StorageFolder externalDevices = Windows.Storage.KnownFolders.RemovableDevices;
                //StorageFolder externalDevices = Windows.Storage.KnownFolders.PicturesLibrary;

                // Get the first child folder, which represents the SD card.
                // var removableFolders = await externalDevices.GetFoldersAsync();
                // StorageFolder sdCard = removableFolders.FirstOrDefault();
                //StorageFolder sdCard = await StorageFolder.GetFolderFromPathAsync("D:\\");
                //StorageFolder sdCard = Windows.Storage.KnownFolders.PicturesLibrary;

                //if (sdCard != null)
                //{
                // An SD card is present and the sdCard variable now contains a reference to it.
                StorageFolder mpoFolder = await  Package.Current.InstalledLocation.GetFolderAsync("Images");
                    if (mpoFolder != null)
                    {
                        /*
                        List<string> fileTypeFilter = new List<string>();
                        fileTypeFilter.Add(".mpo");
                        var queryOptions = new QueryOptions(CommonFileQuery.OrderByName, fileTypeFilter);
                        var query = mpoFolder.CreateFileQueryWithOptions(queryOptions);
                        IReadOnlyList<StorageFile> fileList = await query.GetFilesAsync();
                        */
                        var fileList = await mpoFolder.GetFilesAsync(); // CommonFileQuery.OrderByName);
                        // Process results
                        foreach (StorageFile file in fileList)
                        {
                            // Process file
                        }
                        return fileList;
                    }
                    else
                    {
                        // no MPO folder
                        MessageDialog messageDialog = new MessageDialog("No MPO folder on SD card is present");
                        await messageDialog.ShowAsync();
                    }
                //}
                //else
                //{
                //    // No SD card is present.
                //    MessageDialog messageDialog = new MessageDialog("No SD card is present");
                //    await messageDialog.ShowAsync();
                //}
                return null;
            }
            catch (Exception ex)
            {
                MessageDialog messageDialog = new MessageDialog(ex.Message);
                await messageDialog.ShowAsync();
            }
            return null;
        }

        /*
        // desktop versin see:
        // http://dmitrybrant.com/2011/02/08/the-fujifilm-mpo-3d-photo-format
        // This is converted version for widows phone 8.1
        */
        private async Task OpenMpoFile(string fileName)
        {
            //var images = new List<Image>();
            /*
            FileName = fileName;
            images.Clear();
            cbImage.Items.Clear();
            */
            try
            {
                StorageFile file = await StorageFile.GetFileFromPathAsync(fileName);
                var fileSize = (await file.GetBasicPropertiesAsync()).Size;
                //   this.Cursor = Cursors.WaitCursor;
                byte[] tempBytes = new byte[100];
                using (var fileStream = await file.OpenSequentialReadAsync()) // FileStream(fileName, FileMode.Open, FileAccess.Read))
                {
                    tempBytes = new byte[fileSize];
                    using (DataReader reader = new DataReader(fileStream))
                    {
                        await reader.LoadAsync((uint)fileSize);
                        reader.ReadBytes(tempBytes);
                    }
                }

                List<int> imageOffsets = new List<int>();
                int offset = 0, tempOffset = 0;
                byte[] keyBytes = { 0xFF, 0xD8, 0xFF, 0xE1 };
                byte[] keyBytes2 = { 0xFF, 0xD8, 0xFF, 0xE0 };

                while (true)
                {
                    tempOffset = SearchBytes(tempBytes, keyBytes, offset, tempBytes.Length);
                    if (tempOffset == -1)
                        tempOffset = SearchBytes(tempBytes, keyBytes2, offset, tempBytes.Length);
                    if (tempOffset == -1) break;
                    offset = tempOffset;
                    imageOffsets.Add(offset);
                    offset += 4;
                }

                for (int i = 0; i < imageOffsets.Count; i++)
                {
                    int length;
                    if (i < (imageOffsets.Count - 1))
                        length = imageOffsets[i + 1] - imageOffsets[i];
                    else
                        length = tempBytes.Length - imageOffsets[i];

                    byte[] imageBytes = new byte[length];
                    Array.Copy(tempBytes, imageOffsets[i], imageBytes, 0, length);

                    //var image = new Image();
                    if (i == 0) image1.Source = await ConvertToBitmapImage(imageBytes);
                    if (i == 1) image2.Source = await ConvertToBitmapImage(imageBytes);
                    //images.Add(image);
                }

                if (imageOffsets.Count == 0)
                {
                    MessageDialog messageDialog = new MessageDialog("This does not appear to be a valid MPO file:"+fileName);
                    await messageDialog.ShowAsync();
                }
                else
                {
                    //cbImage.SelectedIndex = 0;
                    //cbMode_SelectedIndexChanged(null, null);
                }

            }
            catch (Exception ex)
            {
                MessageDialog messageDialog = new MessageDialog(ex.Message);
                await messageDialog.ShowAsync();
            }
            finally
            {
                //this.Cursor = Cursors.Default;
            }
        }

        // http://stackoverflow.com/questions/27436237/load-show-convert-image-from-byte-array-database-in-windows-phone-8-1
        public async Task<BitmapImage> ConvertToBitmapImage(byte[] image)
        {
            BitmapImage bitmapimage = null;
            using (InMemoryRandomAccessStream ms = new InMemoryRandomAccessStream())
            {
                using (DataWriter writer = new DataWriter(ms.GetOutputStreamAt(0)))
                {
                    writer.WriteBytes((byte[])image);
                    await writer.StoreAsync();
                }
                bitmapimage = new BitmapImage();
                bitmapimage.SetSource(ms);
            }
            return bitmapimage;
        }

         /*
        // http://dmitrybrant.com/2011/02/08/the-fujifilm-mpo-3d-photo-format
        */
        /// <summary>
        /// Search an array of bytes for a byte pattern specified in another array.
        /// </summary>
        /// <param name="bytesToSearch">Array of bytes to search</param>
        /// <param name="matchBytes">Byte pattern to search for</param>
        /// <param name="startIndex">Starting index within the first array to start searching</param>
        /// <param name="count">Number of bytes in the first array to search</param>
        /// <returns>Zero-based index of the beginning of the byte pattern found in 
        /// the byte array, or -1 if not found.</returns>
        public static int SearchBytes(byte[] bytesToSearch, byte[] matchBytes, int startIndex, int count)
        {
            int ret = -1, max = count - matchBytes.Length + 1;
            bool found;
            for (int i = startIndex; i < max; i++)
            {
                found = true;
                for (int j = 0; j < matchBytes.Length; j++)
                {
                    if (bytesToSearch[i + j] != matchBytes[j]) { found = false; break; }
                }
                if (found) { ret = i; break; }
            }
            return ret;
        }

        /// <summary>
        /// Compare two arrays of bytes.
        /// </summary>
        /// <param name="array1">First array to compare.</param>
        /// <param name="start1">Starting index in the first array to begin comparing.</param>
        /// <param name="array2">Second array to compare.</param>
        /// <param name="start2">Starting index in the second array to begin comparing.</param>
        /// <param name="count">Number of bytes to compare.</param>
        /// <returns>True if the bytes are identical, false otherwise.</returns>
        public static bool MemCmp(byte[] array1, int start1, byte[] array2, int start2, int count)
        {
            bool ret = true;
            for (int i = 0; i < count; i++)
            {
                if (array1[start1] != array2[start2]) { ret = false; break; }
                start1++; start2++;
            }
            return ret;
        }

    }

}
